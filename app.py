from dash import Dash, html, dcc
import plotly.express as px
import pandas as pd
import dota_data
import math
import plotly.graph_objects as go

app = Dash(
    __name__,
    external_stylesheets=['https://codepen.io/chriddyp/pen/bWLwgP.css'],
    )

def visualize_data(df):

    fig_colored_bar = px.bar(df, x="item name", y="winrate", color="pickrate", color_continuous_scale=px.colors.diverging.BrBG)

    fig_grouped_bar = go.Figure(data=[
        go.Bar(name='Win rate', x=df["item name"], y=df["winrate"]),
        go.Bar(name='Pick rate', x=df["item name"], y=df["pickrate"])
    ])

    app.layout = html.Div(children=[
        html.H1(children='Welcome to dota data'),

        # html.Div(children=f"Overall sum from {df[cp.DATE_LABEL].min().date()} to {df[cp.DATE_LABEL].max().date()}:\n\t{math.ceil(df[cp.VALUE_LABEL].sum())}"),

        dcc.Graph(
            id='colored_bar',
            figure=fig_colored_bar
        ),
        dcc.Graph(
            id='grouped_bar',
            figure=fig_grouped_bar
        ),
    ])
    
    app.run_server(host="0.0.0.0", debug=True)

if __name__ == "__main__":
    visualize_data(dota_data.load_df())